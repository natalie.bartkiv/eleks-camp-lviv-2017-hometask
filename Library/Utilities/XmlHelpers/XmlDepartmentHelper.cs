﻿using System.Collections.Generic;
using System.Xml.Linq;
using Library.Models;
using Library.Models.Books;

namespace Library.Utilities.XmlHelpers
{
    class XmlDepartmentHelper
    {
        public static XElement GetXmlDepartment(Department department)
        {
            XElement departmentElement = new XElement("department");

            XElement departmentName = new XElement("name") { Value = department.Name };
            departmentElement.Add(departmentName);

            XElement books = new XElement("books");
            foreach (var book in department.Books)
            {
                var bookElement = XmlBookHelper.GetXmlBookReference(book);
                books.Add(bookElement);
            }
            departmentElement.Add(books);
            return departmentElement;
        }

        public static Department GetDepartment(XElement departmentElement, List<Book> books)
        {
            string departmentName = departmentElement.Element("name")?.Value;
            Department department = new Department(departmentName);
            XElement booksElement = departmentElement.Element("books");
            if (booksElement != null)
            {
                foreach (var bookElement in booksElement.Elements())
                {
                    var idAttribute = bookElement.Attribute("id");
                    if (idAttribute != null)
                    {
                        int bookId = int.Parse(idAttribute.Value);
                        var book = books.Find(b => b.Id == bookId);
                        department.Books.Add(book);
                    }
                }
            }
            return department;
        }
    }
}
