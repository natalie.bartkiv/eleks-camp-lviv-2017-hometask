﻿namespace Matrixes.Utilities
{
    class Swapper
    {
        public static void Swap(ref int one, ref int two)
        {
            int tmp = one;
            one = two;
            two = tmp;
        }

        public static void SwapRows(int [,] matrix, int i, int j)
        {
            int columns = matrix.GetLength(1);
            for (int k = 0; k < columns; ++k)
            {
                Swap(ref matrix[i, k], ref matrix[j, k]);
            }
        }
    }
}
