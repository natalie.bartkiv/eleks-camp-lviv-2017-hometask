﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matrixes.Utilities;

namespace Matrixes.Models
{
    public class Matrix
    {
        private readonly int[,] _data;

        public int Rows => _data.GetLength(0);
        public int Columns => _data.GetLength(1);

        public int this[int row, int column]
        {
            get { return _data[row, column]; }
            set { _data[row, column] = value; }
        }

        public Matrix(int rows, int columns)
        {
            _data = new int[rows, columns];
        }

        public Matrix(int[,] data) : this(data.GetLength(0), data.GetLength(1))
        {
            Array.Copy(data, _data, data.Length);
        }

        public int GetFirstPositiveColumnIndex()
        {
            for (int j = 0; j < Columns; ++j)
            {
                bool allPositive = true;
                for (int i = 0; i < Rows; ++i)
                {
                    if (this[i, j] < 0)
                    {
                        allPositive = false;
                        break;
                    }
                }
                if (allPositive)
                {
                    return j;
                }
            }
            return -1;
        }

        public void SortRowsBySameElementsAmount()
        {
            int []maxesOfSame = new int[Rows];
            for (int i = 0; i < Rows; ++i)
            {
                Dictionary<int, int> sameElementsInRow = new Dictionary<int, int>();
                for (int j = 0; j < Columns; ++j)
                {
                    if (sameElementsInRow.ContainsKey(this[i, j]))
                    {
                        ++sameElementsInRow[this[i, j]];
                    }
                    else
                    {
                        sameElementsInRow[this[i, j]] = 1;
                    }
                }
                maxesOfSame[i] = sameElementsInRow.Values.Max();
            }

            for (int i = 0; i < Rows - 1; ++i)
            {
                int min = i;
                for (int j = i + 1; j < Rows; ++j)
                {
                    if (maxesOfSame[j] < maxesOfSame[min])
                    {
                        min = j;
                    }
                }
                Swapper.Swap(ref maxesOfSame[i], ref maxesOfSame[min]);
                Swapper.SwapRows(_data, i, min); 
            }
        }

        public int GetNotPositiveRowsSum()
        {
            int sum = 0;
            for (int i = 0; i < Rows; ++i)
            {
                int rowSum = 0;
                for (int j = 0; j < Columns; ++j)
                {
                    if (this[i, j] < 0)
                    {
                        for (int k = 0; k < Columns; ++k)
                        {
                            rowSum += this[i, k];
                        }
                        break;
                    }
                }
                sum += rowSum;
            }
            return sum;
        }

        public void DisplaySaddlePoints()
        {
            int[] minsInRows = new int[Rows];
            int[] maxesInColumns = new int[Columns];

            for (int i = 0; i < Rows; ++i)
            {
                minsInRows[i] = int.MaxValue;
                for (int j = 0; j < Columns; ++j)
                {
                    if (this[i, j] < minsInRows[i])
                    {
                        minsInRows[i] = this[i, j];
                    }    
                }
            }

            for (int j = 0; j < Columns; ++j)
            {
                maxesInColumns[j] = int.MinValue;
                for (int i = 0; i < Rows; ++i)
                {
                    if (this[i, j] > maxesInColumns[j])
                    {
                        maxesInColumns[j] = this[i, j];
                    }    
                }
            }

            int saddlePointsCount = 0;
            for (int i = 0; i < Rows; ++i)
            {
                for (int j = 0; j < Columns; ++j)
                {
                    if (this[i, j] == minsInRows[i] && this[i, j] == maxesInColumns[j])
                    {
                        ++saddlePointsCount;
                        Console.WriteLine($"Matrix[{i}, {j}] = {this[i, j]}");
                    }
                }
            }
            if (saddlePointsCount == 0)
            {
                Console.WriteLine("Matrix does not have any saddle points.");
            }
        }

        public void DisplayEqualRowsAndColumns()
        {
            if(Rows != Columns)
                throw new Exception("Matrix has to be square.");

            for (int k = 0; k < Rows; ++k)
            {
                bool equal = true;
                for (int i = 0; i < Columns; ++i)
                {
                    if (this[k, i] != this[i, k])
                    {
                        equal = false;
                        break;
                    }      
                }
                if (equal)
                {
                    Console.Write($"{k}; ");
                }
            }
        }

        public void Display()
        {
            for (int i = 0; i < Rows; ++i)
            {
                for (int j = 0; j < Columns; ++j)
                {
                    Console.Write(this[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public void FillWithRandom(int minValue = -99, int maxValue = 999)
        {
            Random random = new Random((int) DateTime.Now.Ticks);
            for (int i = 0; i < Rows; ++i)
            {
                for (int j = 0; j < Columns; ++j)
                {
                    this[i, j] = random.Next(minValue, maxValue);
                }
            }
        }
    }
}
