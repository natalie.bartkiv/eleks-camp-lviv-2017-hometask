﻿using System.Collections.Generic;
using System.Linq;
using Library.Models;
using Library.Models.Books;
using Library.Models.Interfaces;

namespace Library.Utilities.Extensions
{
    public static class Extensions
    {
        //public static Author HasMostBooks(this List<Author> authors) => authors.Max();
        //public static Department HasMostBooks(this List<Department> departments) => departments.Max();

        public static T HasMostBooks<T>(this List<T> bookContainers) where T : IBookContainer => bookContainers.Max();
        public static Book LeastPages(this List<Book> books) => books.Min();

        public static List<Book> GetBooks<T>(this List<T> bookContainers) where T: IBookContainer
        {
            var books = new List<Book>();
            foreach (var container in bookContainers)
            {
                foreach (var book in container.Books)
                {
                    books.Add(book);
                }
            }
            return books;
        }
    }
}
