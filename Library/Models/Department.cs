﻿using System;
using System.Collections.Generic;
using Library.Models.Books;
using Library.Models.Interfaces;

namespace Library.Models
{
    public class Department: IComparable, ICountingBooks, IBookContainer
    {
        private List<Book> _books;

        public List<Book> Books
        {
            get { return _books; }
            set { _books = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Department(string name)
        {
            this.Name = name;
            Books = new List<Book>();
        }

        public Department() : this("Common")
        {
        }

        public int GetBooksCount()
        {
            return Books.Count;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }

            Department otherDepartment = obj as Department;
            if (otherDepartment != null)
            {
                return GetBooksCount().CompareTo(otherDepartment.GetBooksCount());
            }
            throw new ArgumentException("Object is not a Department");
        }

        public override string ToString()
        {
            return $"{Name} Department";
        }
    }
}
