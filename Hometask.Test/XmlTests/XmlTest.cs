﻿using System;
using System.IO;
using System.Linq;
using Library;
using Library.Models.Books;
using Library.Utilities;
using Library.Utilities.XmlHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hometask.Test.XmlTests
{
    [TestClass]
    public class XmlTest
    {
        private string _filepath = @"../../XmlFiles/LibraryManager.xml";

        [TestMethod]
        public void TestReadXml()
        {
            var manager = XmlHelper.Read(_filepath);
            Console.WriteLine(manager);
        }

        [TestMethod]
        public void TestCreateXml()
        {
            var manager = LibraryManager.GetDefault();
            XmlHelper.CreateXml(manager, _filepath);

            string[] lines = File.ReadAllLines(_filepath);
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
        }

        [TestMethod]
        public void TestAddXmlBook()
        {
            var manager = LibraryManager.GetDefault();
            var bookName = "A Confession";
            string authorName = "Leo Tolstoy";
            int pages = 129;
            int id = 129;
            var author = manager.Authors.FirstOrDefault(a => a.Name == authorName);

            Book book = new Fiction(bookName, author, pages, id);

            XmlBookHelper.AddXmlBook(book, _filepath);
            manager = XmlHelper.Read(_filepath);
            Console.WriteLine(manager);

            Assert.IsTrue(author != null && author.Books.Exists(b => b.Name == bookName));
        }

        [TestMethod]
        public void TestDeleteXmlBook()
        {
            var manager = LibraryManager.GetDefault();
            var bookName = "A Confession";
            string authorName = "Leo Tolstoy";
            int pages = 129;
            int id = 129;
            var author = manager.Authors.FirstOrDefault(a => a.Name == authorName);
               
            Book book = new Fiction(bookName, author, pages, id);

            XmlHelper.DeleteXmlBook(book, _filepath);
            manager = XmlHelper.Read(_filepath);
            Console.WriteLine(manager);
            author = manager.Authors.FirstOrDefault(a => a.Name == authorName);
            Assert.IsFalse(author != null && author.Books.Exists(b => b.Name == bookName));
        }
    }
}
