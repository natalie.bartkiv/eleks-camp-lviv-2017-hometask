﻿using System;
using System.Collections.Generic;
using System.Text;
using Library.Models;
using Library.Models.Books;
using Library.Utilities.Extensions;

namespace Library.Utilities
{
    public class LibraryManager
    {
        private List<Models.Library> _libraries;

        public List<Models.Library> Libraries
        {
            get { return _libraries; }
            set { _libraries = value; }
        }

        private List<Author> _authors;

        public List<Author> Authors
        {
            get { return _authors; }
            set { _authors = value; }
        }

        public LibraryManager()
        {
            Libraries = new List<Models.Library>();
            Authors = new List<Author>();
        }

        public static LibraryManager GetDefault()
        {
            var cervantes = new Author("Migual de Servantes");
            var tolstoy = new Author("Leo Tolstoy");
            var shakespeare = new Author("William Shakespeare");
            var twain = new Author("Mark Twain");
            var orwell = new Author("George Orwell");
            var schildt = new Author("Herbert Schildt");
            var stroustrup = new Author("Bjarne Stroustup");


            Models.Library library = new Models.Library("Vasyl Stefanyk Library", "Lviv, Stefanyka Street, 2");
            var fictionDepartment = new Department("Fiction Books")
            {
                Books = new List<Book>()
                {
                    new Fiction("Don Quixote", cervantes, 992, 0),
                    new Fiction("War and Peace", tolstoy, 1125, 1),
                    new Fiction("Anna Karenina", tolstoy, 864, 2),
                    new Fiction("The Death of Ivan Ilyich", tolstoy, 114, 3),
                    new Fiction("Hamlet", shakespeare, 289, 4),
                    new Fiction("Macbeth", shakespeare, 102, 5),
                    new Fiction("Romeo and Juliet", shakespeare, 95, 6),
                    new Fiction("Juilius Caesar", shakespeare, 115, 7),
                    new Fiction("Richard III", shakespeare, 160, 8),
                    new Fiction("The Adventures of Tom Sawyer", twain, 229, 9),
                    new Fiction("Life on the Missisippi", twain, 435, 10),
                    new Fiction("1984", orwell, 336, 11),
                    new Fiction("Animal Farm", orwell, 112, 12),
                }
            };
            library.Departments.Add(fictionDepartment);

            var itDepartment = new Department("IT")
            {
                Books = new List<Book>()
                {
                new Textbook("A Tour of C++", stroustrup, 450, 13),
                new Textbook("Progremming: Principles and Practices Using C++", stroustrup, 1020, 14),
                new Textbook("The Design and Evolution of C++", stroustrup, 1111, 15),
                new Textbook("C# 6.0: The Complete Reference", schildt, 1200, 16),
                new Textbook("Java 2", schildt, 1000, 17),
                }
            };
            library.Departments.Add(itDepartment);

            LibraryManager manager = new LibraryManager();
            manager.Authors.Add(cervantes);
            manager.Authors.Add(tolstoy);
            manager.Authors.Add(shakespeare);
            manager.Authors.Add(twain);
            manager.Authors.Add(orwell);
            manager.Authors.Add(schildt);
            manager.Authors.Add(stroustrup);
            manager.Libraries.Add(library);
            return manager;
        }

        public void DisplayResults()
        {
            foreach (var library in Libraries)
            {
                library.Departments.Sort();
                Console.WriteLine(library);
                foreach (var department in library.Departments)
                {
                    department.Books.Sort();
                    Console.WriteLine(department);
                    foreach (var book in department.Books)
                    {
                        Console.WriteLine(book);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }

            Console.WriteLine("Authors:");
            Authors.Sort();
            foreach (var author in Authors)
            {
                Console.WriteLine($"{author} (books written: {author.GetBooksCount()})");
            }

            Console.WriteLine();

            Console.WriteLine($"Author with most books written: {Authors.HasMostBooks()}");
            Console.WriteLine($"Department with most books: {Libraries[0].Departments.HasMostBooks()}");
            Console.WriteLine($"Book with least amount of pages: {Libraries[0].Departments[0].Books.LeastPages()}");
            Console.Read();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (var author in Authors)
            {
                builder.AppendLine(author.ToString());
                foreach (var book in author.Books)
                {
                    builder.AppendLine($"\t{book}");
                }
            }
            builder.AppendLine();
            foreach (var library in Libraries)
            {
                builder.AppendLine(library.ToString());
                foreach (var department in library.Departments)
                {
                    builder.AppendLine($"\t{department}");
                    foreach (var book in department.Books)
                    {
                        builder.AppendLine($"\t\t{book}");
                    }
                }
            }

            return builder.ToString();
        }
    }
}
