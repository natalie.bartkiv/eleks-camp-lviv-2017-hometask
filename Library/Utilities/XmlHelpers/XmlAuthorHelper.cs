﻿using System.Xml.Linq;
using Library.Models;

namespace Library.Utilities.XmlHelpers
{
    class XmlAuthorHelper
    {
        public static Author GetAuthor(XElement authorElement)
        {
            if (authorElement != null)
            {
                string name = authorElement.Element("name")?.Value;
                Author author = new Author(name);

                var booksElement = authorElement.Element("books");
                if (booksElement != null)
                {
                    foreach (var bookElement in booksElement.Elements())
                    {
                        XmlBookHelper.GetBook(bookElement, author);
                    }
                }
                return author;
            }
            return null;
        }

        public static XElement GetXmlAuthor(Author author)
        {
            XElement authorElement = new XElement("author");
            XElement authorName = new XElement("name") { Value = author.Name };
            authorElement.Add(authorName);

            XElement books = new XElement("books");
            foreach (var book in author.Books)
            {
                XElement bookElement = XmlBookHelper.GetXmlBook(book);
                books.Add(bookElement);
            }
            authorElement.Add(books);

            return authorElement;
        }
    }
}
