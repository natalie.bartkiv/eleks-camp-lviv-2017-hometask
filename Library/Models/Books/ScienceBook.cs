﻿namespace Library.Models.Books
{
    public class ScienceBook : Book
    {
        private string _subject;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        private string _award;

        public string Award
        {
            get { return _award; }
            set { _award = value; }
        }

        public ScienceBook(string name, Author author, int pages, int id) : base(name, author, pages, id)
        {
        }

        public ScienceBook() { }
    }
}
