﻿using System;
using Matrixes.Models;

namespace Matrixes.Utilities
{
    public class MatrixManager
    {
        public static void DisplayResults()
        {
            DisplayPositiveColumnTask();
            DisplaySortTask();

            DisplaySpecialSumTask();
            DisplaySaddlePointsTask();

            DisplaySpecialDiagonalElementsTask();
        }

        public static Matrix CreateAndDisplayMatrix(int rows, int columns)
        {
            Matrix matrix = new Matrix(rows, columns);
            matrix.FillWithRandom();

            Console.WriteLine("Original matrix:");
            matrix.Display();
            Console.WriteLine();

            return matrix;
        }

        private static void DisplayPositiveColumnTask()
        {
            int rows = 10;
            int columns = 7;

            Matrix matrix = CreateAndDisplayMatrix(rows, columns);
            Console.WriteLine($"First column without negative elements: {matrix.GetFirstPositiveColumnIndex()}");
            Console.WriteLine();
        }

        private static void DisplaySortTask()
        {
            int rows = 10;
            int columns = 5;

            Matrix matrix = new Matrix(rows, columns);
            matrix.FillWithRandom(0, 10);
            Console.WriteLine("Original matrix:");
            matrix.Display();

            Console.WriteLine("Matrix with rows sorted by max same elements amount in each:");
            matrix.SortRowsBySameElementsAmount();
            matrix.Display();
            Console.WriteLine();
        }

        private static void DisplaySpecialSumTask()
        {
            int rows = 5;
            int columns = 7;

            Matrix matrix = CreateAndDisplayMatrix(rows, columns);
            Console.WriteLine($"Sum of rows, which contain at least one negative element: {matrix.GetNotPositiveRowsSum()} ");
            Console.WriteLine();  
        }

        private static void DisplaySaddlePointsTask()
        {
            int rows = 2;
            int columns = 2;

            Matrix matrix = CreateAndDisplayMatrix(rows, columns);
            Console.WriteLine("Saddle points:");
            matrix.DisplaySaddlePoints();
            Console.WriteLine();
        }

        private static void DisplaySpecialDiagonalElementsTask()
        {
            int rows = 8;
            int columns = 8;

            Matrix matrix = CreateAndDisplayMatrix(rows, columns);
            Console.Write("Indexes of equal rows and columns: ");
            matrix.DisplayEqualRowsAndColumns();
        }
    }
}
