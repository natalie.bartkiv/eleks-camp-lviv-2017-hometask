﻿namespace Library.Models.Books
{
    public class Textbook: Book
    {
        private string _subject;

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        private ComplicityLevel _complicityLevel;
        public ComplicityLevel ComplicityLevel {
            get { return _complicityLevel; }
            set { _complicityLevel = value; }
        }

        public Textbook(string name, Author author, int pages, int id) : base(name, author, pages, id)
        {
        }

        public Textbook() { }
    }

    public enum ComplicityLevel {Beginner, Intermediate, Advanced }
}
