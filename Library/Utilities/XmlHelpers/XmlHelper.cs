﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Library.Models;
using Library.Models.Books;
using Library.Utilities.Extensions;

namespace Library.Utilities.XmlHelpers
{
    public class XmlHelper
    {
        public static XDocument Open(string filepath)
        {
            XDocument document = XDocument.Load(filepath);

            if (IsEmpty(document))
            {
                throw new Exception("File is empty!");
            }
            return document;
        }

        public static bool IsEmpty(XDocument document)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            XElement rootElement = document.Element("libraryManager");
            if (rootElement != null && rootElement.HasElements)
            {
                return false;
            }
            return true;
        }

        public static LibraryManager Read(string filepath)
        {
            LibraryManager manager = new LibraryManager();
            XDocument document = Open(filepath);

            XElement rootElement = document.Element("libraryManager");
            manager.Authors = GetAuthors(rootElement);
            List<Book> books = manager.Authors.GetBooks();
            manager.Libraries = GetLibraries(rootElement, books);

            return manager;
        }

        public static List<Author> GetAuthors(XElement rootElement)
        {
            XElement authorsElement = rootElement.Element("authors");
            List<Author> authors = new List<Author>();
            if (authorsElement != null)
            {
                foreach (var authorElement in authorsElement.Elements("author"))
                {
                    var author = XmlAuthorHelper.GetAuthor(authorElement);
                    authors.Add(author);
                }
            }
            return authors;
        }

        public static List<Models.Library> GetLibraries(XElement rootElement, List<Book> books)
        {
            XElement librariesElement = rootElement.Element("libraries");
            var libraries = new List<Models.Library>();
            if (librariesElement != null)
            {
                foreach (var libraryElement in librariesElement.Elements("library"))
                {
                    string name = libraryElement.Element("name")?.Value;
                    string address = libraryElement.Element("address")?.Value;
                    Models.Library library = new Models.Library(name, address);

                    XElement departmentsElement = libraryElement.Element("departments");
                    if (departmentsElement != null)
                    {
                        foreach (var departmentElement in departmentsElement.Elements("department"))
                        {
                            var department = XmlDepartmentHelper.GetDepartment(departmentElement, books);
                            library.Departments.Add(department);
                        }
                    }
                    libraries.Add(library);
                }
            }
            return libraries;
        }

        public static void CreateXml(LibraryManager manager, string filepath)
        {
            XElement rootElement = new XElement("libraryManager");
            rootElement.Add(GetXmlAuthors(manager.Authors));
            rootElement.Add(GetXmlLibraries(manager.Libraries));

            XDocument document = new XDocument(rootElement);
            document.Save(filepath);
        }
       
        public static XElement GetXmlAuthors(List<Author> authors)
        {
            XElement rootElement = new XElement("authors");
            foreach (var author in authors)
            {
                XElement authorElement = XmlAuthorHelper.GetXmlAuthor(author);
                rootElement.Add(authorElement);
            }
            return rootElement;
        }

        public static XElement GetXmlLibraries(List<Models.Library> libraries)
        {
            XElement rootElement = new XElement("libraries");
            foreach (var library in libraries)
            {
                XElement libraryElement = new XElement("library");
                XElement libararyName = new XElement("name") {Value = library.Name};
                libraryElement.Add(libararyName);

                XElement libararyAddress = new XElement("address") {Value = library.Address};
                libraryElement.Add(libararyAddress);

                XElement departmentsElement = new XElement("departments");
                foreach (var department in library.Departments)
                {
                    XElement departmentElement = XmlDepartmentHelper.GetXmlDepartment(department);
                    departmentsElement.Add(departmentElement);
                }
                libraryElement.Add(departmentsElement);
                rootElement.Add(libraryElement);
            }
            return rootElement;
        }

        public static void DeleteXmlBook(Book book, string filepath)
        {
            XmlBookHelper.DeleteXmlBook(book, filepath);
            XmlBookHelper.DeleteXmlBookReference(book, filepath);
        }
    }
}
