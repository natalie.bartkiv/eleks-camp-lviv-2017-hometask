﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Library.Models;
using Library.Models.Books;

namespace Library.Utilities.XmlHelpers
{
    public static class XmlBookHelper
    {
        public static XElement GetXmlBook(Book book)
        {
            Type bookType = book.GetType();
            XElement bookElement = new XElement(bookType.ToString());

            bookElement.SetAttributeValue("id", book.Id);

            XElement bookName = new XElement("name") { Value = book.Name };
            bookElement.Add(bookName);

            XElement bookAuthor = new XElement("author") { Value = book.Author.Name };
            bookElement.Add(bookAuthor);

            XElement bookPages = new XElement("pages") { Value = book.Pages.ToString() };
            bookElement.Add(bookPages);

            return bookElement;
        }

        public static XElement GetXmlBookReference(Book book)
        {
            Type bookType = book.GetType();
            XElement bookElement = new XElement(bookType.ToString());
            bookElement.SetAttributeValue("id", book.Id);

            return bookElement;
        }

        public static Book GetBook(XElement bookElement, Author author)
        {
            Type type = Type.GetType(bookElement.Name.ToString(), false, true);
            var idAttribute = bookElement.Attribute("id");
            var pagesAttribute = bookElement.Element("pages");
            if (idAttribute != null && pagesAttribute != null)
            {
                int id = int.Parse(idAttribute.Value);
                int pages = int.Parse(pagesAttribute.Value);
                string name = bookElement.Element("name")?.Value;

                var book = Activator.CreateInstance(type);
                type.GetProperty("Name").SetValue(book, name);
                type.GetProperty("Pages").SetValue(book, pages);
                type.GetProperty("Id").SetValue(book, id);
                type.GetProperty("Author").SetValue(book, author);

                return (Book) book;
            }
            return null;
        }

        public static void AddXmlBook(Book book, string filepath)
        {
            XDocument document = XmlHelper.Open(filepath);
            XElement rootElement = document.Element("libraryManager");
            if (rootElement != null && rootElement.HasElements)
            {
                var authorsElement = rootElement.Element("authors");
                if (authorsElement != null)
                {
                    IEnumerable<XElement> authorElements = authorsElement.Elements("author");

                    foreach (var authorElement in authorElements)
                    {
                        if (authorElement.Element("name")?.Value == book.Author.Name)
                        {
                            var booksElement = authorElement.Element("books");
                            if (booksElement != null && !booksElement.Elements().Where(IsABook(book)).Any())
                            {
                                var bookElement = GetXmlBook(book);
                                booksElement.Add(bookElement);
                            }
                            break;
                        }
                    }
                }
            }
            document.Save(filepath);
        }

        public static void DeleteXmlBook(Book book, string filepath)
        {
            XDocument document = XmlHelper.Open(filepath);

            XElement rootElement = document.Element("libraryManager");
            if (rootElement != null && rootElement.HasElements)
            {
                var authorsElement = rootElement.Element("authors");
                if (authorsElement != null)
                {
                    IEnumerable<XElement> authorElements = authorsElement.Elements("author");
                    foreach (var authorElement in authorElements)
                    {
                        if (authorElement.Element("name")?.Value == book.Author.Name)
                        {
                            var booksElement = authorElement.Element("books");
                            if (booksElement != null)
                            {
                                var bookElement = booksElement.Elements().Where(IsABook(book)).FirstOrDefault();
                                bookElement?.Remove();
                                document.Save(filepath);
                            }
                            break;
                        }
                    }
                }
            }
        }

        public static void DeleteXmlBookReference(Book book, string filepath)
        {
            XDocument document = XmlHelper.Open(filepath);
            XElement rootElement = document.Element("libraryManager");

            XElement librariesElement = rootElement?.Element("libraries");
            if (librariesElement != null)
            {
                bool bookDeleted = false;
                foreach (var libraryElement in librariesElement.Elements("library"))
                {
                    XElement departmentsElement = libraryElement.Element("departments");
                    if (departmentsElement != null)
                    {
                        foreach (var departmentElement in departmentsElement.Elements("department"))
                        {
                            XElement booksElement = departmentElement.Element("books");
                            var bookElement = booksElement?.Elements().FirstOrDefault(IsABook(book));
                            bookElement?.Remove();
                            document.Save(filepath);
                            bookDeleted = true;
                            break;
                        }
                    }
                    if (bookDeleted)
                    {
                        break;
                    }
                }
            }
        }

        public static Func<XElement, bool> IsABook(Book book)
        {
            return b => b.Name == book.GetType().ToString() && b.Attribute("id")?.Value == book.Id.ToString();
        }
    }
}
