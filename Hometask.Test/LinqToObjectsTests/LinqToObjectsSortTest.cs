﻿using System;
using System.Collections.Generic;
using Library;
using Library.Models;
using Library.Models.Books;
using Library.Utilities;
using Library.Utilities.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hometask.Test.LinqToObjectsTests
{
    [TestClass]
    public class LinqToObjectsSortTest
    {
        [TestMethod]
        public void TestSortingByBooksCount()
        {
            var manager = LibraryManager.GetDefault();
            var sortedAuthors = manager.Authors.SortByBooksCount();
            foreach (var author in sortedAuthors)
            {
                Console.WriteLine($"{author} [BooksCount: {author.GetBooksCount()}]");
            }
        }

        [TestMethod]
        public void TestSortingBooksByAuthor()
        {
            var books = new List<Book>()
            {
                new Fiction("Test book 1", new Author("James"), 222, 1),
                new Fiction("Test book 2", new Author("Vincent"), 222, 2),
                new Fiction("Test book 3", new Author("Jerry"), 222, 3),
                new Fiction("Test book 4", new Author("Tom"), 222, 4),
                new Fiction("Test book 5", new Author("Dan"), 222, 5),
                new Fiction("Test book 6", new Author("Ben"), 222, 6),
                new Fiction("Test book 7", new Author("Maggie"), 222, 7),
            };

            books = books.SortByAuthor();
            foreach (var book in books)
            {
                Console.WriteLine(book);
            }
        }

        [TestMethod]
        public void TestSortingBooksByPagesCount()
        {
            var manager = LibraryManager.GetDefault();
            var books = manager.Authors.GetBooks();
            books = books.SortByPagesCount();

            foreach (var book in books)
            {
                Console.WriteLine(book);
            }
        }

        [TestMethod]
        public void TestSortingBooksById()
        {
            var manager = LibraryManager.GetDefault();
            var books = manager.Authors.GetBooks();
            books = books.SortById();

            foreach (var book in books)
            {
                Console.WriteLine($"{book.Id}. {book}");
            }
        }
    }
}
