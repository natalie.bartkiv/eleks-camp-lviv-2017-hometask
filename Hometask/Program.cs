﻿using System;
using Library;
using Library.Utilities;
using Matrixes;
using Matrixes.Utilities;

namespace Hometask
{
    class Program
    {
        static void Main(string[] args)
        {
            LibraryManager manager = LibraryManager.GetDefault();
            manager.DisplayResults();

            Console.Read();
            Console.Clear();

            MatrixManager.DisplayResults();
            Console.Read();
        }
    }
}
