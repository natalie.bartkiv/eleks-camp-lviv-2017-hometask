﻿using System;
using System.Collections.Generic;
using Library.Models.Books;
using Library.Models.Interfaces;

namespace Library.Models
{
    public class Author: IComparable, ICountingBooks, IBookContainer
    {
        private List<Book> _books;

        public List<Book> Books
        {
            get { return _books; }
            set { _books = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            private set { _name = value; }
        }

        public Author() : this("Unknown")
        {
        }

        public Author(string name)
        {
            Name = name;
            Books = new List<Book>();
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }

            Author otherAuthor = obj as Author;
            if (otherAuthor != null)
            {
                return GetBooksCount().CompareTo(otherAuthor.GetBooksCount());
            }
            throw new ArgumentException("Object is not an Author");
        }

        public int GetBooksCount()
        {
            return Books.Count;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
