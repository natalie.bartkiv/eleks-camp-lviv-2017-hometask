﻿using System.Collections.Generic;
using System.Linq;
using Library.Models.Books;
using Library.Models.Interfaces;

namespace Library.Utilities.Extensions
{
    public static class SortExtensions
    {
        public static List<T> SortByBooksCount<T>(this List<T> bookContainers) where T : IBookContainer
        {
            return bookContainers.OrderBy(c => c.Books.Count).ToList();
        }

        public static List<Book> SortByAuthor(this List<Book> books)
        {
            return books.OrderBy(b => b.Author.Name).ToList();
        }

        public static List<Book> SortByPagesCount(this List<Book> books)
        {
            return books.OrderBy(b => b.Pages).ToList();
        }

        public static List<Book> SortById(this List<Book> books)
        {
            return books.OrderBy(b => b.Id).ToList();
        }
    }
}
