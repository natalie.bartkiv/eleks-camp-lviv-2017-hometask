﻿using System.Collections.Generic;
using Library.Models.Books;

namespace Library.Models.Interfaces
{
    public interface IBookContainer
    {
        List<Book> Books { get; set; }
    }
}
