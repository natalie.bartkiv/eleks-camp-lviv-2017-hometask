﻿using System;
using System.Collections.Generic;
using Library.Models.Interfaces;

namespace Library.Models
{
    public class Library:IComparable, ICountingBooks
    {
        private List<Department> _departments;

        public List<Department> Departments
        {
            get { return _departments; }
            private set { _departments = value; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
            private set { _name = value; }
        }

        private string _address;

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public Library(string name, string address = "Unknown")
        {
            Name = name;
            Address = address;
            Departments = new List<Department>();
        }

        public Library() : this("Public Library") { }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }

            Library otherLibrary = obj as Library;
            if (otherLibrary != null)
            {
                return GetBooksCount().CompareTo(otherLibrary.GetBooksCount());
            }
            throw new ArgumentException("Object is not a Library");
        }

        public int GetBooksCount()
        {
            int count = 0;
            foreach (var department in Departments)
            {
                count += department.GetBooksCount();
            }
            return count;
        }

        public override string ToString()
        {
            return $"{Name} (Address: {Address})";
        }
    }
}
