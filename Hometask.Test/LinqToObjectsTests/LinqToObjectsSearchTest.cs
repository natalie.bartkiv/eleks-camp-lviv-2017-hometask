﻿using System;
using System.Linq;
using Library.Utilities;
using Library.Utilities.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hometask.Test.LinqToObjectsTests
{
    [TestClass]
    public class LinqToObjectsSearchTest
    {
        [TestMethod]
        public void TestSearchingBookByAuthor()
        {
            var books = LibraryManager.GetDefault().Authors.GetBooks();
            string authorName = "Leo Tolstoy";
            var book = books.SearchByAuthor(authorName);

            Console.WriteLine(book);
            Assert.IsTrue(book != null);
        }

        [TestMethod]
        public void TestSearchingBookInLibarary()
        {
            var library = LibraryManager.GetDefault().Libraries.First();
            string bookName = "War and Peace";

            var book = library.SearchBook(bookName);
            Console.WriteLine(book);
            Assert.IsTrue(book != null);
        }

        [TestMethod]
        public void TestSearchingBookById()
        {
            var books = LibraryManager.GetDefault().Authors.GetBooks();
            int id = new Random().Next(books.Count);

            Console.WriteLine($"Id = {id}");
            var book = books.SearchById(id);

            Console.WriteLine(book);
            Assert.IsTrue(book != null);
        }
    }
}
