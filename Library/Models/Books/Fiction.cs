﻿namespace Library.Models.Books
{
    public class Fiction: Book
    {
        private string _genre;

        public string Genre
        {
            get { return _genre; }
            set { _genre = value; }
        }

        public Fiction(string name, Author author, int pages, int id) : base(name, author, pages, id)
        {
        }

        public Fiction()  { }
    }
}
