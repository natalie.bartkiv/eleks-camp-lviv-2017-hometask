﻿using System;

namespace Library.Models.Books
{
    public abstract class Book : IComparable
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private Author _author;

        public Author Author
        {
            get { return _author; }
            set
            {
                
                _author = value;
                if (value != null)
                {
                    _author.Books.Add(this);
                }
            }
        }

        private int _pages;

        public int Pages
        {
            get { return _pages; }
            set { _pages = value; }
        }

        protected Book(string name, Author author, int pages, int id)
        {
            this.Name = name;
            this.Pages = pages;
            this.Author = author;
            this.Id = id;
        }

        protected Book() : this("Unknown", null, 0, -1) { }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }

            Book otherBook = obj as Book;
            if (otherBook != null)
            {
                return Pages.CompareTo(otherBook.Pages);
            }
            throw new ArgumentException("Object is not a Book");
        }

        public override string ToString()
        {
            return $"{Name} by {Author} ({Pages} pages)";
        }
    }

}
