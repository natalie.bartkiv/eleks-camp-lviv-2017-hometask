﻿namespace Library.Models.Interfaces
{
    public interface ICountingBooks
    {
        int GetBooksCount();
    }
}
