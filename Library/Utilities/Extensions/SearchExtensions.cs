﻿using System.Collections.Generic;
using System.Linq;
using Library.Models.Books;

namespace Library.Utilities.Extensions
{
    public static class SearchExtensions
    {
        public static Book SearchByAuthor(this List<Book> books, string authorName)
        {
            return books.First(b => b.Author.Name == authorName);
        }

        public static Book SearchBook(this Models.Library libarary, string bookName)
        {
            foreach (var department in libarary.Departments)
            {
                var book = department.Books.First(b => b.Name == bookName);
                if (book != null)
                {
                    return book;
                }
            }
            return null;
        }

        public static Book SearchById(this List<Book> books, int id)
        {
            return books.First(b => b.Id == id);
        }
    }
}
